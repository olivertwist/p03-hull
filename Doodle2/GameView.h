//
//  GameView.h
//  Doodle2
//
//  Created by Patrick Madden on 2/4/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Jumper.h"
#import "Brick.h"
#import <CoreMotion/CoreMotion.h>
#import <AVFoundation/AVFoundation.h>
#import <AudioToolbox/AudioToolbox.h>


@interface GameView : UIView {

}
@property (nonatomic, strong) Jumper *jumper;
@property (nonatomic, strong) NSMutableArray *bricks;
@property (nonatomic, strong) NSMutableArray *clouds;
@property (nonatomic) float tilt;
@property (nonatomic) UILabel* scoreLabel;
@property (nonatomic)UIButton *tryAgain;

@property (nonatomic) int score;
-(void)arrange:(CADisplayLink *)sender;

@end
