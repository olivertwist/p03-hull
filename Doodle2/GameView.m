//
//  GameView.m
//  Doodle2
//
//  Created by Patrick Madden on 2/4/17.
//  Copyright © 2017 Binghamton CSD. All rights reserved.
//

#import "GameView.h"

@implementation GameView
@synthesize jumper, bricks,clouds;
@synthesize tilt;

-(id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self)
    {
 
        
        _score = 0;
        CGRect bounds = [self bounds];
        _scoreLabel = [[UILabel alloc] initWithFrame:CGRectMake(bounds.size.width*.02, bounds.size.width*.01, bounds.size.width*.37, bounds.size.height*.05)];
        [_scoreLabel setFont:[UIFont fontWithName:@"Marker Felt" size:20.0f]];
        
        
    
        [_scoreLabel setBackgroundColor:[UIColor redColor]];
        _scoreLabel.text = [NSString stringWithFormat:@"Score: %d",_score];
        
        
        [self addSubview:_scoreLabel];
        [self bringSubviewToFront:_scoreLabel];
        jumper = [[Jumper alloc] initWithFrame:CGRectMake(bounds.size.width/2, bounds.size.height/2, 75, 75)];
        UIImage *image = [UIImage imageNamed: @"madden_patrick.png"];

        [jumper setImage:image];
        
        
        [jumper setBackgroundColor:[UIColor clearColor]];
        [jumper setDx:0];
        [jumper setDy:0];
        [self addSubview:jumper];
        [self makeBricks:nil];
        UIColor *myColor = [UIColor colorWithRed: 0.0/255.0 green: 191.0/255.0 blue:255.0/255.0 alpha: 1.0];
        [self setBackgroundColor:myColor];
        clouds = [[NSMutableArray alloc] init];
        int cloudNum;
        for(int i = 0; i < 4; i++)
        {
            cloudNum = 3;
            UIImageView *cloud = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,200,100)];
            UIImage *cloudImage = [UIImage imageNamed:[NSString stringWithFormat:@"cloud%d.png",cloudNum]];
            [cloud setImage:cloudImage];
            [cloud setCenter:CGPointMake(rand() % (int)(bounds.size.width), .1+(bounds.size.height)*i*.2)];
            [clouds addObject:cloud];
            [self addSubview:cloud];
            [self sendSubviewToBack:cloud];
        }
        
    }
    return self;
}

-(IBAction)makeBricks:(id)sender
{
    CGRect bounds = [self bounds];
    float width = bounds.size.width * .2;
    float height = bounds.size.height * .05;
    
    if (bricks)
    {
        for (Brick *brick in bricks)
        {
            [brick removeFromSuperview];
        }
    }
    bricks = [[NSMutableArray alloc] init];

    Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
    UIImage *image = [UIImage imageNamed: @"BLOCK.png"];

    [b setImage:image];
    [self addSubview:b];
    [b setCenter:CGPointMake((int)(bounds.size.width * .5), (int)(bounds.size.height * .6))];
    [bricks addObject:b];
    for (int i = 0; i < 9; ++i)
        {
            Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
            [b setImage:image];
            [self addSubview:b];
            [b setCenter:CGPointMake(rand() % (int)(bounds.size.width * .8), rand() % (int)(bounds.size.height * .95))];
            [bricks addObject:b];
        }
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
-(void) shiftBricksDown
{
    CGRect bounds = [self bounds];


        
        
        [UIView animateWithDuration:.5f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
            
            for (Brick *brick in bricks) [brick setCenter:CGPointMake(brick.center.x, brick.center.y+(bounds.size.height)*.5)];
            
            for (UIImageView* cloud in clouds) [cloud setCenter:CGPointMake(cloud.center.x, cloud.center.y+(bounds.size.height)*.5)];
            
        } completion:^(BOOL finished) {
            
            for (Brick *brick in bricks) if(brick.center.y > bounds.size.height*1.1){
                [brick removeFromSuperview];
                [brick setCenter:CGPointMake(brick.center.x, brick.center.y+(bounds.size.height)*.5)];
            }
            for (UIImageView * cloud in clouds) if(cloud.center.y >bounds.size.height) [cloud removeFromSuperview];
        }];
        
        
        
    
}
-(void) gameOver
{
    CGRect bounds = [self bounds];


    _tryAgain = [[UIButton alloc]initWithFrame:CGRectMake((bounds.size.width*.2), (bounds.size.height*.3), (bounds.size.width*.6), bounds.size.width*.3)];
    UIImage * restart = [UIImage imageNamed:@"restartButton.png"];
    
    [_tryAgain setImage:restart forState:UIControlStateNormal];
    [_tryAgain addTarget:self action:@selector(resetGame) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:_tryAgain];
    
}
-(void) resetGame
{
    CGRect bounds = [self bounds];

    _score = 0;
    _scoreLabel.text = [NSString stringWithFormat:@"Score: %d",_score];
    [jumper setDx:0];
    [jumper setDy:0];
    [UIView animateWithDuration:.5f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
        

        
    } completion:^(BOOL finished) {
        [jumper setCenter:CGPointMake(bounds.size.height/2, bounds.size.width/2)];
        [self addSubview:jumper];
        [self makeBricks:nil];

 
    }];

    
}

-(void)arrange:(CADisplayLink *)sender
{
    float loc;
    CFTimeInterval ts = [sender timestamp];
    UIImage *image = [UIImage imageNamed: @"BLOCK.png"];

    CGRect bounds = [self bounds];
 
    float width = bounds.size.width * .2;
    float height = bounds.size.height*.05;
    // Apply gravity to the acceleration of the jumper
    [jumper setDy:[jumper dy] - 1.3];
    
    // Apply the tilt.  Limit maximum tilt to + or - 5
    [jumper setDx:[jumper dx] + tilt];
    if ([jumper dx] > 5)
        [jumper setDx:5];
    if ([jumper dx] < -5)
        [jumper setDx:-5];
    
    // Jumper moves in the direction of gravity
    CGPoint p = [jumper center];
    p.x += [jumper dx];
    p.y -= [jumper dy];
    
    // If the jumper has fallen below the bottom of the screen,
    // add a positive velocity to move him
    if (p.y > bounds.size.height)
    {
        [jumper setDy:0];
        [jumper setDx:0];
        
        
        [UIView animateWithDuration:.8f delay:0.0f options:UIViewAnimationOptionCurveLinear animations:^{
            for (Brick *brick in bricks) [brick setCenter:CGPointMake(brick.center.x, brick.center.y-(bounds.size.height))];
            for (UIImageView *cloud in clouds) [cloud setCenter:CGPointMake(cloud.center.x, cloud.center.y-(bounds.size.height))];
        } completion:^(BOOL finished) {
            
            for (Brick *brick in bricks) [brick removeFromSuperview];
            for (UIImageView *cloud in clouds) [cloud removeFromSuperview];
        }];
        
    }
    if (p.y > bounds.size.height*1.1)
    {
        [self gameOver];

    }
    
    // If we've gone past the top of the screen, wrap around
    if (p.y < bounds.size.height*.5 && [jumper dy] < 0)
    {

        
    }
    
    
    // If we have gone too far left, or too far right, wrap around
    if (p.x < 0)
        p.x += bounds.size.width;
    if (p.x > bounds.size.width)
        p.x -= bounds.size.width;
    
    // If we are moving down, and we touch a brick, we get
    // a jump to push us up.
    if ([jumper dy] < 0)
    {
        for (Brick *brick in bricks)
        {
            CGRect b = [brick frame];
            if (CGRectContainsPoint(b, CGPointMake(p.x, p.y+(22))))
            {
                // Yay!  Bounce!
                NSLog(@"Bounce!");
                [jumper setDy:17];
                
                [self shiftBricksDown];
                
                int cloudNum;
                int cloudX;
            
                for (int i = 0; i < 4; ++i)
                {
                    cloudNum = 1 + (int)arc4random() % 2;
                    cloudX = rand() %(int)(bounds.size.width);
                    UIImageView *cloud = [[UIImageView alloc]initWithFrame:CGRectMake(0,0,200,100)];
                    UIImage *cloudImage = [UIImage imageNamed:[NSString stringWithFormat:@"cloud%d.png",cloudNum]];
                    [cloud setImage:cloudImage];
                    [cloud setCenter:CGPointMake(cloudX, -1*((bounds.size.height)*i*.1))];
                    [clouds addObject:cloud];
                    [self addSubview:cloud];
                    [self sendSubviewToBack:cloud];

                    
                    
                    
                    
                    Brick *b = [[Brick alloc] initWithFrame:CGRectMake(0, 0, width, height)];
                    [self addSubview:b];
                    [b setImage:image];
                    loc = rand() %(int)(bounds.size.width * .8);
                    [b setCenter:CGPointMake(loc, -1*(int)(bounds.size.height * .5))];
                    
                    [UIView animateWithDuration:.5f delay:0.0f
                                        options:UIViewAnimationOptionCurveLinear animations:^(void){

                        [b setCenter:CGPointMake(loc, (bounds.size.height * (i/10.0)))];
                                            [cloud setCenter:CGPointMake(cloudX, (.1+(bounds.size.height)*i*.1))];
                    } completion:^(BOOL finished) {}];

                    [bricks addObject:b];
                }

                _score += 50;
                _scoreLabel.text = [NSString stringWithFormat:@"Score: %d",_score];
                [self bringSubviewToFront:_scoreLabel];
                
                
                break;
                
            }
        }
    }
    
    
    [jumper setCenter:p];
    // NSLog(@"Timestamp %f", ts);
}

@end
